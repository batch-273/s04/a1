class Student {
  constructor(name, email, grades) {
    this.name = name;
    this.email = email;
    this.gradeAve = undefined;
    this.passed = undefined;
    this.passedWithHonors = undefined;

    if (grades.length === 4) {
      if (grades.every((grade) => grade >= 0 && grade <= 100)) {
        this.grades = grades;
      } else {
        this.grades = undefined;
      }
    } else {
      this.grades = undefined;
    }
  }

  login() {
    console.log(`${this.email} has logged in`);
    return this;
  }
  logout() {
    console.log(`${this.email} has logged out`);
    return this;
  }
  listGrades() {
    console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    return this;
  }
  computeAve() {
    let sum = 0;
    this.grades.forEach((grade) => (sum = sum + grade));
    this.gradeAve = sum / 4;
    return this;
  }
  willPass() {
    this.passed = this.computeAve().gradeAve >= 85 ? true : false;
    return this;
  }

  willPassWithHonors() {
    if (this.passed) {
      if (this.gradeAve >= 90) {
        this.passedWithHonors = true;
      } else {
        this.passedWithHonors = false;
      }
    } else {
      this.passedWithHonors = false;
    }
    return this;
  }
}

class Section {
  constructor(name) {
    this.name = name;
    this.students = [];
    this.honorStudents = undefined;
    this.honorsPercentage = undefined;
  }
  addStudent(name, email, grades) {
    this.students.push(new Student(name, email, grades));
    return this;
  }
  countHonorStudents() {
    let count = 0;
    this.students.forEach((student) => {
      if (
        student.computeAve().willPass().willPassWithHonors().passedWithHonors
      ) {
        count++;
      }
    });
    this.honorStudents = count;
    return this;
  }
  computeHonorsPercentage() {
    this.honorsPercentage =
      (this.honorStudents / this.honorStudents.length) * 100;
    return this;
  }
}

const section1A = new Section("section1A");
// section1A.addStudent("John", "john@mail.com", [89, 84, 78, 88]);
section1A.addStudent("Joe", "joe@mail.com", [78, 82, 79, 85]);
section1A.addStudent("Jane", "jane@mail.com", [87, 89, 91, 93]);
section1A.addStudent("Jessie", "jessie@mail.com", [91, 89, 92, 93]);
console.log(section1A);

class Grade {
  constructor(level) {
    this.level = level;
    this.section = [];
    this.totalStudents = 0;
    this.totalHonorStudents = 0;
    this.batchAveGrade = undefined;
    this.batchMinGrade = undefined;
    this.batchMaxGrade = undefined;
  }
  addSection(section) {
    this.section.push(section);
    return this;
  }
  countStudents() {
    let count = 0;
    this.section.forEach((section) => {
      // console.log(section.length);
      count += section.students.length;
    });
    this.totalStudents = count;
    return this;
  }
  countHonorStudents() {
    let count = 0;
    this.section.forEach((sec) => {
      // console.log(sec.students);
      sec.students.forEach((student) => {
        if (student.willPass().willPassWithHonors().passedWithHonors) {
          count++;
        }
      });
    });
    this.totalHonorStudents = count;
    return this;
  }
  computeBatchAve() {
    let aveTot = 0;
    this.section.forEach((sec) => {
      sec.students.forEach((student) => {
        aveTot += student.computeAve().gradeAve;
        // console.log(aveTot);
      });
    });
    this.batchAveGrade = aveTot / this.countStudents().totalStudents;
    return this;
  }
  getBatchMinGrade() {
    let batchGrades = [];
    this.section.forEach((sec) => {
      sec.students.forEach((student) => {
        batchGrades.push(student.computeAve().gradeAve);
        // console.log(aveTot);
      });
    });
    this.batchMinGrade = Math.min.apply(Math, batchGrades);
    return this;
  }
  getBatchMaxGrade() {
    let batchGrades = [];
    this.section.forEach((sec) => {
      sec.students.forEach((student) => {
        batchGrades.push(student.computeAve().gradeAve);
        // console.log(aveTot);
      });
    });
    this.batchMaxGrade = Math.max.apply(Math, batchGrades);
    return this;
  }
}

console.log("[Item 1]");
let grade1 = new Grade(1);
console.log(grade1);
console.log("[Item 2]");
console.log(grade1.addSection(section1A));
console.log("[Item 3]");
console.log(
  grade1.section
    .find((section) => section.name === "section1A")
    .addStudent("John", "john@mail.com", [89, 84, 78, 88])
);
console.log("[Item 4]");
console.log(grade1.countStudents());

console.log("[Item 5]");
console.log(grade1.countHonorStudents());

console.log("[Item 6]");
console.log(grade1.computeBatchAve());

console.log("[Item 7]");
console.log(grade1.getBatchMinGrade());

console.log("[Item 8]");
console.log(grade1.getBatchMaxGrade());
